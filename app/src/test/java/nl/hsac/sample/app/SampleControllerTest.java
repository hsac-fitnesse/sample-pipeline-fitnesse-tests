package nl.hsac.sample.app;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SampleControllerTest {
    private SampleController controller = new SampleController();

    @Test
    public void getGreeting() {
        assertEquals("Hello John", controller.getGreeting("John"));
    }
}
