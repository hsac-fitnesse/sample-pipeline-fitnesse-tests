package nl.hsac.sample.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SampleController.class);

    @Value("${app.version}")
    private String version;

    @GetMapping("/version")
    public String getVersion() {
        return version;
    }

    @GetMapping("/hello/{name}")
    public String getGreeting(@PathVariable(value = "name") String name) {
        return "Hello " + name;
    }
}
