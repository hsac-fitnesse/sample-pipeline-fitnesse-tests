#!/bin/sh

rm -rf target
docker-compose up -d sample-app
docker-compose up tests
mkdir target
docker cp tests:/fitnesse/target/. target/.
docker logs sample-app > target/sample-app.log 2>&1
docker-compose down

docker create --name tests-combine hsac/fitnesse-fixtures-combine:latest
docker cp target/. tests-combine:/fitnesse/target/

docker start tests-combine
docker logs -f tests-combine

docker cp tests-combine:/fitnesse/target/fitnesse-results/. target/combined
docker rm -f -v tests-combine
