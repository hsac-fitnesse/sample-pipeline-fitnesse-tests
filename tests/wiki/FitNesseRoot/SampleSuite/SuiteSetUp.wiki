---
Help: Defines where to look for fixture code.
---
This set up page is executed once, before any tests in this suite.

An [[import table][.FitNesse.UserGuide.WritingAcceptanceTests.SliM.ImportTable]] defines which Java packages may contain fixture classes.

|Import                       |
|nl.hsac.fitnesse.fixture     |
|nl.hsac.fitnesse.fixture.slim|

Wait for application to start

|script                         |http test           |
|set stop test on exception     |false               |
|repeat at most                 |200     |times      |
|show                           |get from|${URL}/    |
|repeat until response status is|200                 |
|show                           |time spent repeating|
