variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_TLS_CERTDIR: ""
  DOCKER_DRIVER: overlay2
  FIXTURE_VERSION: 4.12.1

stages:
  - compile
  - test
  - post-test

before_script:
  - BUILD_VERSION=$CI_PIPELINE_ID-$CI_COMMIT_SHORT_SHA-$CI_COMMIT_REF_NAME
  - APP_IMAGE=${CI_REGISTRY_IMAGE}/sample-app:${BUILD_VERSION}

compile-and-verify-project:
  stage: compile
  image: maven:3.6-jdk-11
  cache:
    paths:
      - .m2/repository
  variables:
    # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
    # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
    MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
    # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
    # when running from the command line.
    # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
    MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"
  script:
    - APP_SHA=`git log -1 --pretty=%h app`
    - echo ${APP_SHA}
    - cd app
    - mvn $MAVEN_CLI_OPTS clean test jib:build
      -P !local-dev
      -Djib.container.environment=app.version="${BUILD_VERSION}"
      -Djib.to.image=$APP_IMAGE
      -Djib.to.tags=$APP_SHA,latest
      -Djib.to.auth.username=$CI_REGISTRY_USER -Djib.to.auth.password=$CI_REGISTRY_PASSWORD

.fitnesse: &test_definition
  except:
    - tags
  image:
    name: docker/compose:1.25.0
    entrypoint: ["/bin/sh", "-c"]
  services:
    - docker:19.03.1-dind
  stage: test
  dependencies: []
  needs:
    - compile-and-verify-project
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - export RUN_NAME=$CI_JOB_NAME
    - export APP_VERSION=$BUILD_VERSION
    - export APP_IMAGE=$APP_IMAGE
    - export CONTAINER=tests
    - export APP=sample-app

    # create cache image for test container so we don't have to pull from docker hub for each run
    - export TEST_IMAGE=${CI_REGISTRY_IMAGE}/fitnesse-${RUN_NAME}:${FIXTURE_VERSION}
    - docker pull ${TEST_IMAGE}
      || (docker pull ${FITNESSE_IMAGE} && docker tag ${FITNESSE_IMAGE} ${TEST_IMAGE} && docker push ${TEST_IMAGE})

    # start application and test, wait for completion
    - cd tests
    - docker-compose up -d ${APP}
    - docker-compose up -d ${CONTAINER}
    - docker logs -f ${CONTAINER}

    # extract test results (so they can become artifacts)
    - mkdir -p target/${RUN_NAME}
    - docker cp ${CONTAINER}:/fitnesse/target/. target/${RUN_NAME}/.
    - mkdir target/fitnesse-results
    - mv target/${RUN_NAME}/fitnesse-results target/fitnesse-results/${RUN_NAME}
    - docker ps -a

    # give job exit code of test container (failed tests mark job as failed)
    - exit `docker inspect ${CONTAINER} --format='{{.State.ExitCode}}'`
  after_script:
    - APP=sample-app
    - CONTAINER=tests
    - cd tests

    # extract docker logs (so they can become job artifacts)
    - docker logs ${APP} &> target/$CI_JOB_NAME/${APP}.log
    - docker logs ${CONTAINER} &> target/$CI_JOB_NAME/fitnesse.log

    # cleanup by removing all containers
    - docker-compose down
  artifacts:
    when: always
    expire_in: 1 week
    paths:
      - tests/target
    reports:
      junit: tests/target/$CI_JOB_NAME/failsafe-reports/*.xml

test-1:
  <<: *test_definition
  variables:
    SUITE: SampleSuite.BackEndTests
    FITNESSE_IMAGE: hsac/fitnesse-fixtures-test-jre8:$FIXTURE_VERSION

test-2:
  <<: *test_definition
  variables:
    SUITE: SampleSuite.FrontEndTests
    FITNESSE_IMAGE: hsac/fitnesse-fixtures-test-jre8-chrome:$FIXTURE_VERSION

.needs-all-tests: &after_all_tests
  stage: post-test
  needs: &all_tests
    - test-1
    - test-2

reports-combine:
  <<: *after_all_tests
  when: always
  dependencies: *all_tests
  image:
    name: hsac/fitnesse-fixtures-combine:$FIXTURE_VERSION
    entrypoint: [""]
  variables:
    GIT_STRATEGY: none
  script:
    - /fitnesse/hsac-html-report-generator tests/target/fitnesse-results test-results
  artifacts:
    when: always
    expire_in: 1 week
    paths:
      - test-results
